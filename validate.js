$("#commentform").validate({
    rules: {
        email: {
            required: true,
            email: true
        },
        comment: {
            required: true
        }
    },
    messages: {
        email: {
            required: "Your email address is required!",
            email: "Your email address must be in the format of name@domain.com"
        },
        comment: {
            required: "You must leave a comment!"
        }
    }
});

$('#comment').keyup(function() {
    var maxLength = $(this).attr('maxlength');
    var charsInput = $(this).val().length
    var textLen = maxLength - charsInput;
    var value = textLen*100/maxLength;
    $('#charscounter').text(textLen);
    $('#progress').animate({'width': value + '%'}, 1)
});
